<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog turistic</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>

<table width="75%" align="center" border="0" cellspacing="0" cellpadding="0">
    <?php
    include "config.php";
    include "functions.php";
    include "header.php";
    $sir = $_GET['criteriu'];
    $ident = $_GET['id'];
    /* Acest script se foloseste in cazul in care pentru un criteriu de cautare apar
       mai multe locatii.
       De exemplu pentru criteriul "insula" apar 6 locatii.
    */
    ?>

    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="background-color:lightgoldenrodyellow;" width="80%">
                        <?php
                        $dest = display($sir,$ident);
                        ?>
                    </td>
                    <td style="background-color:lightblue;text-align:center;vertical-align:top;" width="20%">
                        <div class="sd">
                            <?php
                            include "sidebar.php";  ?>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <?php include "footer.php"; ?>
        </td>
    </tr>

</table>

</body>
</html>