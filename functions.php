<?php

function menu($lan){    //afisare header
    include "config.php";
    $res = mysqli_query($mysqlConnect,"SELECT * FROM meniu WHERE language='$lan'");
    if (!$mysqlConnect) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $meniu = $res->fetch_all(MYSQLI_ASSOC);

    ?>
    <ul>
        <?php foreach ($meniu as $menLine): ?>
            <li><a href= "<?php echo $menLine['url']; ?>"> <?php echo $menLine['title']; ?></a></li>
        <?php endforeach; ?>
    </ul>
    <?php
}

function sidebar($des) {  // afisare sidebar
    include "config.php";
    $result = mysqli_query($mysqlConnect, "SELECT * FROM locations WHERE destination='$des' ");
    $locatie = $result->fetch_all(MYSQLI_ASSOC);
    foreach ($locatie as $line) {
        $ident = $line['Id']; // numele locatiei
        echo "<a href=\"destinatie.php?Id=".$ident."\">".$line['name']."</a><br>";
    }
}

function footer() {

    include "config.php";
    $res = mysqli_query($mysqlConnect, "SELECT * FROM footer");
    if (!$mysqlConnect) {
        die("Connection failed: " . mysqli_connect_error());
    }
    $footer = $res->fetch_all(MYSQLI_ASSOC);
    foreach ($footer as $line) {
        echo "<td><a style='font-size:100%;' href='#'>" . $line['name'] . "</a></td>";
    }
}

function afisare($id) {

    include "config.php";
    $result = mysqli_query($mysqlConnect,"SELECT * FROM locations WHERE Id='$id' ");
    $loc = $result->fetch_all(MYSQLI_ASSOC);
    if ($loc[0]['destination'] == 'intern') {
       $valuta = 'LEI';
    }
    else {$valuta = 'EUR';}
    $dst = $loc[0]['destination'];

    ?>
<div>
    <img src="<?php echo $loc[0]['image']; ?>" width="600">
    <div class="c1"> <?php echo $loc[0]['description']; ?> <br><br>
        Pretul aproximativ este <?php echo $loc[0]['price'].$valuta; ?>
    </div>
</div>
<?php
    return $dst;
}  ?>

<?php

function cauta($sh) {

    include "config.php";
    $result = mysqli_query($mysqlConnect,"SELECT * FROM locations WHERE name='$sh' or description LIKE '%$sh%' ");
    $loc = $result->fetch_all(MYSQLI_ASSOC);
    $nrLoc = count($loc);

    if ($nrLoc == 1) {
        if ($loc[0]['destination'] == 'intern') {
            $valuta = 'LEI';
        }
        else { $valuta = 'EUR'; }

        $dst = $loc[0]['destination'];

        ?>
        <div>
            <img src="<?php echo $loc[0]['image']; ?>" width="600">
            <div class="c1"> <?php echo $loc[0]['description']; ?> <br><br>
                Pretul aproximativ este <?php echo $loc[0]['price'] . $valuta; ?>
            </div>
        </div>
        <?php
        return $dst;
    }
    if ($nrLoc > 1) {
        if ($loc[0]['destination'] == 'intern') {
            $valuta = 'LEI';
        } else {
            $valuta = 'EUR';
        }
        $dst = $loc[0]['destination'];

        ?>
        <div>
            <img src="<?php echo $loc[0]['image']; ?>" width="600">
            <div class="c1"> <?php echo $loc[0]['description']; ?> <br><br>
                Pretul aproximativ este <?php echo $loc[0]['price'] . $valuta; ?>
            </div>
            <div>
                <ul>
                    <?php foreach ($loc as $line): ?>
                        <li><a style="color:darkcyan;font-size:75%;" href="display.php?criteriu=<?php echo $sh; ?>&id=<?php echo $line['Id']; ?> "> <?php echo $line['name']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>

        <?php
        return $dst;
    }
    else {echo " <h4> Nu s-a gasit nici o locatie.Introduceti alta valoare. <h4> ";}
}

function display($sh,$id)
{
/*  Aceasta functie se foloseste in cazul in care pentru un criteriu de cautare apar
    mai multe locatii.
    De exemplu pentru criteriul "insula" apar 6 locatii.
 */
    include "config.php";
    $result = mysqli_query($mysqlConnect, "SELECT * FROM locations WHERE name='$sh' or description LIKE '%$sh%' ");
    $loc = $result->fetch_all(MYSQLI_ASSOC);

    $res = mysqli_query($mysqlConnect, "SELECT * FROM locations WHERE Id='$id' ");
    $place = $res->fetch_all(MYSQLI_ASSOC);

    if ($place[0]['destination'] == 'intern') {
        $valuta = 'LEI';
    } else {
        $valuta = 'EUR';
    }

    $dst = $place[0]['destination'];

    ?>
    <div>
        <img src="<?php echo $place[0]['image']; ?>" width="600">
        <div class="c1"> <?php echo $place[0]['description']; ?> <br><br>
            Pretul aproximativ este <?php echo $place[0]['price'] . $valuta; ?>
        </div>
        <div>
            <ul>
                <?php foreach ($loc as $line): ?>
                    <li><a style="color:darkcyan;font-size:75%;"
                           href="display.php?criteriu=<?php echo $sh; ?>&id=<?php echo $line['Id']; ?> "> <?php echo $line['name']; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <?php
    return $dst;
}
    ?>








