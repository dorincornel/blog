<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog turistic</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<table width="75%" align="center" border="0" cellspacing="0" cellpadding="0">
    <?php
    include "config.php";
    include "functions.php";
    include "header.php";
    $sir = $_GET['criteriu'];
    ?>

    <tr>
        <td>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="background-color:lightgoldenrodyellow;" width="80%">
                        <?php
                        $dest = cauta($sir);
                        ?>
                    </td>
                    <td style="background-color:lightblue;text-align:center;vertical-align:top;" width="20%">
                        <div class="sd">
                            <?php
                            include "sidebar.php";  ?>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <?php include "footer.php"; ?>
        </td>
    </tr>

</table>

</body>
</html>