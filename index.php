<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Blog turistic</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<table width="75%" align="center" border="0" cellspacing="0" cellpadding="0">
    <?php
    include "config.php";
    include "functions.php";
    include "header.php"; ?>

    <tr>
        <td>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="background-color:lightgoldenrodyellow;" width="80%">
                    <div>
                    <img class="c1" src="images/ibiza.jpg" width="400">
                     <div class="c1">  Acesta este un blog turistic.<br>
                        Prezinta cateva destinatii turistice atractive atat din tara cat si din afara.
                        Fiecare destinatie are atasata o imagine si o scurta descriere.
                     </div>
                    </div>

                </td>
                <td style="background-color:lightblue;text-align:center;" width="20%">
                    <div>
                    <b>Destinatii</b><br><br>
                    Sibiu<br>
                    Mamaia<br>
                    Corfu<br>
                    Tenerife<br><br>
                    <b> Alte destinatii</b><br><br>
                    Paralia<br>
                    Ibiza<br>
                    Cuba<br>
                    Cancun
                    </div>
                </td>
            </tr>
        </table>
        </td>
    </tr>
    <tr>
        <td>
       <?php include "footer.php"; ?>
        </td>
    </tr>

</table>

</body>
</html>